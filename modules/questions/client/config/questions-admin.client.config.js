(function () {
  'use strict';

  // Configuring the Articles Admin module
  angular
    .module('questions.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addMenuItem('topbar', {
      title: 'Questions',
      state: 'admin.questions',
      type: 'dropdown',
      roles: ['admin']
    });

    Menus.addSubMenuItem('topbar', 'admin.questions', {
      title: 'Manage Questions',
      state: 'admin.questions.list'
    });

    Menus.addSubMenuItem('topbar', 'admin.questions', {
      title: 'Create Questions',
      state: 'admin.questions.create'
    });
  }
}());
