(function () {
  'use strict';

  describe('Segments Route Tests', function () {
    // Initialize global variables
    var $scope,
      SegmentsService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _SegmentsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      SegmentsService = _SegmentsService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('segments');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/segments');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          SegmentsController,
          mockSegment;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('segments.view');
          $templateCache.put('modules/segments/client/views/view-segment.client.view.html', '');

          // create mock Segment
          mockSegment = new SegmentsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Segment Name'
          });

          // Initialize Controller
          SegmentsController = $controller('SegmentsController as vm', {
            $scope: $scope,
            segmentResolve: mockSegment
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:segmentId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.segmentResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            segmentId: 1
          })).toEqual('/segments/1');
        }));

        it('should attach an Segment to the controller scope', function () {
          expect($scope.vm.segment._id).toBe(mockSegment._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/segments/client/views/view-segment.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          SegmentsController,
          mockSegment;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('segments.create');
          $templateCache.put('modules/segments/client/views/form-segment.client.view.html', '');

          // create mock Segment
          mockSegment = new SegmentsService();

          // Initialize Controller
          SegmentsController = $controller('SegmentsController as vm', {
            $scope: $scope,
            segmentResolve: mockSegment
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.segmentResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/segments/create');
        }));

        it('should attach an Segment to the controller scope', function () {
          expect($scope.vm.segment._id).toBe(mockSegment._id);
          expect($scope.vm.segment._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/segments/client/views/form-segment.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          SegmentsController,
          mockSegment;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('segments.edit');
          $templateCache.put('modules/segments/client/views/form-segment.client.view.html', '');

          // create mock Segment
          mockSegment = new SegmentsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Segment Name'
          });

          // Initialize Controller
          SegmentsController = $controller('SegmentsController as vm', {
            $scope: $scope,
            segmentResolve: mockSegment
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:segmentId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.segmentResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            segmentId: 1
          })).toEqual('/segments/1/edit');
        }));

        it('should attach an Segment to the controller scope', function () {
          expect($scope.vm.segment._id).toBe(mockSegment._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/segments/client/views/form-segment.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
