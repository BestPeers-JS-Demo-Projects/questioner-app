'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Segment = mongoose.model('Segment'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  segment;

/**
 * Segment routes tests
 */
describe('Segment CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Segment
    user.save(function () {
      segment = {
        name: 'Segment name'
      };

      done();
    });
  });

  it('should be able to save a Segment if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Segment
        agent.post('/api/segments')
          .send(segment)
          .expect(200)
          .end(function (segmentSaveErr, segmentSaveRes) {
            // Handle Segment save error
            if (segmentSaveErr) {
              return done(segmentSaveErr);
            }

            // Get a list of Segments
            agent.get('/api/segments')
              .end(function (segmentsGetErr, segmentsGetRes) {
                // Handle Segments save error
                if (segmentsGetErr) {
                  return done(segmentsGetErr);
                }

                // Get Segments list
                var segments = segmentsGetRes.body;

                // Set assertions
                (segments[0].user._id).should.equal(userId);
                (segments[0].name).should.match('Segment name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Segment if not logged in', function (done) {
    agent.post('/api/segments')
      .send(segment)
      .expect(403)
      .end(function (segmentSaveErr, segmentSaveRes) {
        // Call the assertion callback
        done(segmentSaveErr);
      });
  });

  it('should not be able to save an Segment if no name is provided', function (done) {
    // Invalidate name field
    segment.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Segment
        agent.post('/api/segments')
          .send(segment)
          .expect(400)
          .end(function (segmentSaveErr, segmentSaveRes) {
            // Set message assertion
            (segmentSaveRes.body.message).should.match('Please fill Segment name');

            // Handle Segment save error
            done(segmentSaveErr);
          });
      });
  });

  it('should be able to update an Segment if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Segment
        agent.post('/api/segments')
          .send(segment)
          .expect(200)
          .end(function (segmentSaveErr, segmentSaveRes) {
            // Handle Segment save error
            if (segmentSaveErr) {
              return done(segmentSaveErr);
            }

            // Update Segment name
            segment.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Segment
            agent.put('/api/segments/' + segmentSaveRes.body._id)
              .send(segment)
              .expect(200)
              .end(function (segmentUpdateErr, segmentUpdateRes) {
                // Handle Segment update error
                if (segmentUpdateErr) {
                  return done(segmentUpdateErr);
                }

                // Set assertions
                (segmentUpdateRes.body._id).should.equal(segmentSaveRes.body._id);
                (segmentUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Segments if not signed in', function (done) {
    // Create new Segment model instance
    var segmentObj = new Segment(segment);

    // Save the segment
    segmentObj.save(function () {
      // Request Segments
      request(app).get('/api/segments')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Segment if not signed in', function (done) {
    // Create new Segment model instance
    var segmentObj = new Segment(segment);

    // Save the Segment
    segmentObj.save(function () {
      request(app).get('/api/segments/' + segmentObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', segment.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Segment with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/segments/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Segment is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Segment which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Segment
    request(app).get('/api/segments/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Segment with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Segment if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Segment
        agent.post('/api/segments')
          .send(segment)
          .expect(200)
          .end(function (segmentSaveErr, segmentSaveRes) {
            // Handle Segment save error
            if (segmentSaveErr) {
              return done(segmentSaveErr);
            }

            // Delete an existing Segment
            agent.delete('/api/segments/' + segmentSaveRes.body._id)
              .send(segment)
              .expect(200)
              .end(function (segmentDeleteErr, segmentDeleteRes) {
                // Handle segment error error
                if (segmentDeleteErr) {
                  return done(segmentDeleteErr);
                }

                // Set assertions
                (segmentDeleteRes.body._id).should.equal(segmentSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Segment if not signed in', function (done) {
    // Set Segment user
    segment.user = user;

    // Create new Segment model instance
    var segmentObj = new Segment(segment);

    // Save the Segment
    segmentObj.save(function () {
      // Try deleting Segment
      request(app).delete('/api/segments/' + segmentObj._id)
        .expect(403)
        .end(function (segmentDeleteErr, segmentDeleteRes) {
          // Set message assertion
          (segmentDeleteRes.body.message).should.match('User is not authorized');

          // Handle Segment error error
          done(segmentDeleteErr);
        });

    });
  });

  it('should be able to get a single Segment that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Segment
          agent.post('/api/segments')
            .send(segment)
            .expect(200)
            .end(function (segmentSaveErr, segmentSaveRes) {
              // Handle Segment save error
              if (segmentSaveErr) {
                return done(segmentSaveErr);
              }

              // Set assertions on new Segment
              (segmentSaveRes.body.name).should.equal(segment.name);
              should.exist(segmentSaveRes.body.user);
              should.equal(segmentSaveRes.body.user._id, orphanId);

              // force the Segment to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Segment
                    agent.get('/api/segments/' + segmentSaveRes.body._id)
                      .expect(200)
                      .end(function (segmentInfoErr, segmentInfoRes) {
                        // Handle Segment error
                        if (segmentInfoErr) {
                          return done(segmentInfoErr);
                        }

                        // Set assertions
                        (segmentInfoRes.body._id).should.equal(segmentSaveRes.body._id);
                        (segmentInfoRes.body.name).should.equal(segment.name);
                        should.equal(segmentInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Segment.remove().exec(done);
    });
  });
});
