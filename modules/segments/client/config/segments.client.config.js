(function () {
  'use strict';

  angular
    .module('segments')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    // menuService.addMenuItem('topbar', {
    //   title: 'Segments',
    //   state: 'segments',
    //   type: 'dropdown',
    //   roles: ['*']
    // });
    //
    // // Add the dropdown list item
    // menuService.addSubMenuItem('topbar', 'segments', {
    //   title: 'List Segments',
    //   state: 'segments.list'
    // });
    //
    // // Add the dropdown create item
    // menuService.addSubMenuItem('topbar', 'segments', {
    //   title: 'Create Segment',
    //   state: 'segments.create',
    //   roles: ['user']
    // });
  }
}());
