(function() {
  'use strict';

  // Configuring the Articles Admin module
  angular
    .module('segments.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addMenuItem('topbar', {
      title: 'Segments',
      state: 'admin.segments',
      type: 'dropdown',
      roles: ['admin']
    });
    Menus.addSubMenuItem('topbar', 'admin.segments', {
      title: 'Manage Segments',
      state: 'admin.segments.list'
    });

    Menus.addSubMenuItem('topbar', 'admin.segments', {
      title: 'Create Segments',
      state: 'admin.segments.create'
    });
  }
}());
