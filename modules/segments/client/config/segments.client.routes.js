(function () {
  'use strict';

  angular
    .module('segments')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('segments', {
        abstract: true,
        url: '/segments',
        template: '<ui-view/>'
      })
      .state('segments.list', {
        url: '',
        templateUrl: '/modules/segments/client/views/list-segments.client.view.html',
        controller: 'SegmentsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Segments List'
        }
      })
      .state('segments.create', {
        url: '/create',
        templateUrl: '/modules/segments/client/views/form-segment.client.view.html',
        controller: 'SegmentsController',
        controllerAs: 'vm',
        resolve: {
          segmentResolve: newSegment
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Segments Create'
        }
      })
      .state('segments.edit', {
        url: '/:segmentId/edit',
        templateUrl: '/modules/segments/client/views/form-segment.client.view.html',
        controller: 'SegmentsController',
        controllerAs: 'vm',
        resolve: {
          segmentResolve: getSegment
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Segment {{ segmentResolve.name }}'
        }
      })
      .state('segments.view', {
        url: '/:segmentId',
        templateUrl: '/modules/segments/client/views/view-segment.client.view.html',
        controller: 'SegmentsController',
        controllerAs: 'vm',
        resolve: {
          segmentResolve: getSegment
        },
        data: {
          pageTitle: 'Segment {{ segmentResolve.name }}'
        }
      });
  }

  getSegment.$inject = ['$stateParams', 'SegmentsService'];

  function getSegment($stateParams, SegmentsService) {
    return SegmentsService.get({
      segmentId: $stateParams.segmentId
    }).$promise;
  }

  newSegment.$inject = ['SegmentsService'];

  function newSegment(SegmentsService) {
    return new SegmentsService();
  }
}());
