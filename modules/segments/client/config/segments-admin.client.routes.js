(function () {
  'use strict';

  angular
    .module('segments.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.segments', {
        abstract: true,
        url: '/segments',
        template: '<ui-view/>'
      })
      .state('admin.segments.list', {
        url: '',
        templateUrl: '/modules/segments/client/views/admin/list-segments.client.view.html',
        controller: 'SegmentsAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.segments.create', {
        url: '/create',
        templateUrl: '/modules/segments/client/views/admin/form-segment.client.view.html',
        controller: 'SegmentsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          segmentResolve: newSegment
        }
      })
      .state('admin.segments.edit', {
        url: '/:segmentId/edit',
        templateUrl: '/modules/segments/client/views/admin/form-segment.client.view.html',
        controller: 'SegmentsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin'],
          pageTitle: '{{ segmentResolve.title }}'
        },
        resolve: {
          segmentResolve: getSegment
        }
      });
  }

  getSegment.$inject = ['$stateParams', 'SegmentsService'];

  function getSegment($stateParams, SegmentsService) {
    return SegmentsService.get({
      segmentId: $stateParams.segmentId
    }).$promise;
  }

  newSegment.$inject = ['SegmentsService'];

  function newSegment(SegmentsService) {
    return new SegmentsService();
  }
}());
