

// Segments service used to communicate Segments REST endpoints

(function() {
  'use strict';

  angular
    .module('segments.services')
    .factory('SegmentsService', SegmentsService);

  SegmentsService.$inject = ['$resource', '$log'];

  function SegmentsService($resource, $log) {
    var Segment = $resource('/api/segments/:segmentId', {
      segmentId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(Segment.prototype, {
      createOrUpdate: function() {
        var segment = this;
        return createOrUpdate(segment);
      }
    });

    return Segment;

    function createOrUpdate(segment) {
      if (segment._id) {
        return segment.$update(onSuccess, onError);
      } else {
        return segment.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(segment) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
