(function () {
  'use strict';

  angular
    .module('segments.admin')
    .controller('SegmentsAdminListController', SegmentsAdminListController);

  SegmentsAdminListController.$inject = ['SegmentsService'];

  function SegmentsAdminListController(SegmentsService) {
    var vm = this;

    vm.segments = SegmentsService.query();
  }
}());
