(function () {
  'use strict';

  angular
    .module('segments.admin')
    .controller('SegmentsAdminController', SegmentsAdminController);

  SegmentsAdminController.$inject = ['$scope', '$state', '$window', 'segmentResolve', 'Authentication', 'Notification', 'QuestionsService'];

  function SegmentsAdminController($scope, $state, $window, segment, Authentication, Notification, QuestionsService) {
    var vm = this;

    vm.segment = segment;
    vm.authentication = Authentication;
    vm.form = {};
    vm.error = null;
    vm.remove = remove;
    vm.save = save;
    $scope.example1model = [];
    vm.addRandom = addRandom ;
    vm.segment.questions = [];

    vm.showAddedRandom = false ;

    $scope.example1data = QuestionsService.query();
    // console.log($scope.example1data);

    $scope.exampleSettings = {displayProp: 'question', idProp: '_id'};

    function addRandom(lim){
      QuestionsService.query(function(data){
        console.log(data);
    var result =     data.slice(0, lim).map(function () {
            return this.splice(Math.floor(Math.random() * this.length), 1)[0];
        }, data.slice());

        console.log(result);

        // add question to segment
        for(var i = 0; i < result.length ; i++ ){
          vm.segment.questions.push(result[i]);
        }
        console.log(vm.segment.questions) ;
      });
      vm.showAddedRandom = true ;
    }

    //angular-bootstrap-multiselect

    vm.options = QuestionsService.query(function(data){
      console.log(data);
      data.slice(0, 3).map(function () {
          return this.splice(Math.floor(Math.random() * this.length), 1)[0];
      }, data.slice());

    });


    // segment category array

    vm.categories = [{
        'category': 'Multi-Optional'
      },
      {
        'category': 'Multi-Correct & Multi-Otional'
      },
      {
        'category': 'True-False '
      }
    ]



    // Remove existing Segment
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.segment.$remove(function () {
          $state.go('admin.segments.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Segment deleted successfully!' });
        });
      }
    }

    // Save Segment
    function save(isValid) {
      console.log('called');

      vm.segment.category = vm.selected_category.category;

      if (!isValid) {
        console.log(vm.segment);
        $scope.$broadcast('show-errors-check-validity', 'vm.form.segmentForm');
        return false;
      }

      // Create a new segment, or update the current instance
      vm.segment.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        console.log(res);
        $state.go('admin.segments.list'); // should we send the User to the list or the updated Segment's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Segment saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Segment save error!' });
      }
    }
  }
}());
