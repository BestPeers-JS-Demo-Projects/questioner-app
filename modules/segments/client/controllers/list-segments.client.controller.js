(function () {
  'use strict';

  angular
    .module('segments')
    .controller('SegmentsListController', SegmentsListController);

  SegmentsListController.$inject = ['SegmentsService'];

  function SegmentsListController(SegmentsService) {
    var vm = this;

    vm.segments = SegmentsService.query();
  }
}());
