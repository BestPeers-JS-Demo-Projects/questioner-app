(function () {
  'use strict';

  // Segments controller
  angular
    .module('segments')
    .controller('SegmentsController', SegmentsController);

  SegmentsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'segmentResolve'];

  function SegmentsController ($scope, $state, $window, Authentication, segment) {
    var vm = this;

    vm.authentication = Authentication;
    vm.segment = segment;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing Segment
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.segment.$remove($state.go('segments.list'));
      }
    }

    // Save Segment
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.segmentForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.segment._id) {
        vm.segment.$update(successCallback, errorCallback);
      } else {
        vm.segment.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('segments.view', {
          segmentId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
