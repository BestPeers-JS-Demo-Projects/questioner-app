
(function (app) {
  'use strict';

  app.registerModule('segments', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('segments.admin', ['core.admin']);
  app.registerModule('segments.admin.routes', ['core.admin.routes']);
  app.registerModule('segments.services');
  app.registerModule('segments.routes', ['ui.router', 'core.routes', 'segments.services']);
}(ApplicationConfiguration));
