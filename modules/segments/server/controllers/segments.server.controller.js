'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Segment = mongoose.model('Segment'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Segment
 */
exports.create = function(req, res) {
  var segment = new Segment(req.body);
  segment.user = req.user;
  console.log(req.body);

  segment.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(segment);
    }
  });
};

/**
 * Show the current Segment
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var segment = req.segment ? req.segment.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  segment.isCurrentUserOwner = req.user && segment.user && segment.user._id.toString() === req.user._id.toString();

  res.jsonp(segment);
};

/**
 * Update a Segment
 */
exports.update = function(req, res) {
  var segment = req.segment;

  segment = _.extend(segment, req.body);

  segment.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(segment);
    }
  });
};

/**
 * Delete an Segment
 */
exports.delete = function(req, res) {
  var segment = req.segment;

  segment.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(segment);
    }
  });
};

/**
 * List of Segments
 */
exports.list = function(req, res) {
  Segment.find().sort('-created').populate('user', 'displayName').populate('questions', 'question options').exec(function(err, segments) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(segments);
    }
  });
};

/**
 * Segment middleware
 */
exports.segmentByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Segment is invalid'
    });
  }

  Segment.findById(id).populate('user', 'displayName').populate('questions', 'question options').exec(function (err, segment) {
    if (err) {
      return next(err);
    } else if (!segment) {
      return res.status(404).send({
        message: 'No Segment with that identifier has been found'
      });
    }
    req.segment = segment;
    next();
  });
};
