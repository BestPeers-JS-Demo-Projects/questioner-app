'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Segment Schema
 */
var SegmentSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Segment name',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  questions: [{
    type: Schema.ObjectId,
    ref: 'Question'
  }],
  category: {
    type: String
  },
  isMinusMarking: {
    type: Boolean,
    default: false
  },
  time_limit: {
    type: Number
  },
  segment_type: {
    type: String
  }
});

mongoose.model('Segment', SegmentSchema);
