'use strict';

/**
 * Module dependencies
 */
var segmentsPolicy = require('../policies/segments.server.policy'),
  segments = require('../controllers/segments.server.controller');

module.exports = function(app) {
  // Segments Routes
  app.route('/api/segments').all(segmentsPolicy.isAllowed)
    .get(segments.list)
    .post(segments.create);

  app.route('/api/segments/:segmentId').all(segmentsPolicy.isAllowed)
    .get(segments.read)
    .put(segments.update)
    .delete(segments.delete);

  // Finish by binding the Segment middleware
  app.param('segmentId', segments.segmentByID);
};
