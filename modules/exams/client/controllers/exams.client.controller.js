(function () {
  'use strict';

  // Exams controller
  angular
    .module('exams')
    .controller('ExamsController', ExamsController);

  ExamsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'examResolve', 'ExamsService', '$stateParams'];

  function ExamsController ($scope, $state, $window, Authentication, exam, ExamsService, $stateParams) {
    var vm = this;

    vm.authentication = Authentication;
    vm.exam = exam;
    console.log(vm.exam);
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.startExamSegment = startExamSegment
    vm.activeQuestion = 0; // currently active question in the quiz
    vm.error = false; // error flag. Will be set when user tries to finish quiz with
    vm.finalise = false;
    vm.setActiveQuestion = setActiveQuestion ;
    vm.newExam = newExam;
    vm.segment = $stateParams.examSegment ;
    vm.selectAnswer = selectAnswer ;
    vm.isAnswered = isAnswered;
    vm.isCorrect = isCorrect ;
    vm.questionAnswered = questionAnswered ;

    function newExam(){
      for(var i = 0 ; i < vm.exam.segments.length ; i ++){
        for(var j=0 ; j < vm.exam.segments[i].questions.length; j++){
          vm.exam.segments[i].questions[j].selected = null ;
          vm.exam.segments[i].questions[j].correct = null ;
        }

      }
    }

    vm.newExam();
    console.log(vm.exam.segments);

    // Remove existing Exam
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.exam.$remove($state.go('exams.list'));
      }
    }

    //start exam segment

    function startExamSegment(index){
      console.log(index);
      vm.examSegment = vm.exam.segments[index] ;
      console.log(vm.examSegment);
      $state.go('exams.startSegment', {
        examId: vm.exam._id,
        examSegment: vm.examSegment
      });
    }

    //show Active Questions
    function setActiveQuestion(index){
            // no argument passed, data = undefined.
            if(index === undefined){
                var breakOut = false;

                var quizLength = vm.segment.questions.length - 1;

                while(!breakOut){
                    vm.activeQuestion = vm.activeQuestion < quizLength?++vm.activeQuestion:0;
                    if(vm.activeQuestion === 0){
                        vm.error = true;
                    }

                    if(vm.segment.questions[vm.activeQuestion].selected === null){
                        breakOut = true;
                    }
                }
            }else{
                vm.activeQuestion = index;
            }

        }
// to continue answering question

function questionAnswered(){

    var quizLength = vm.segment.questions.length;

    var numQuestionsAnswered = 0;

    for(var x = 0; x < quizLength; x++){
        if(vm.segment.questions[vm.activeQuestion].selected !== null){
            numQuestionsAnswered++;
            if(numQuestionsAnswered >= quizLength){
                // final check to ensure all questions are actuall answered
                for(var i = 0; i < quizLength; i++){
                    /*
                     * if find a question that is not answered, set it to
                     * active question then return from this function
                     * to ensure finalise flag is not set
                     */
                    if(vm.segment.questions[i].selected === null){
                        setActiveQuestion(i);
                        return;
                    }
                }
                // set finalise flag and remove any existing warnings
                vm.error = false;
                vm.finalise = true;
                return;
            }
        }
    }

    /*
     * There are still questions to answer so increment to next
     * unanswered question using the setActiveQuestion method
     */
    vm.setActiveQuestion();
}

// select answer
function selectAnswer(index){
          vm.segment.questions[vm.activeQuestion].selected = index ;
      }

//


    function isAnswered (index) {
        var answered = 'Not Answered';
        vm.segments.questions[index].options.forEach(function (element, index, array) {
            if (element.Selected == true) {
                answered = 'Answered';
                return false;
            }
        });
        return answered;
    };

     function isCorrect (question) {
        var result = 'correct';
        question.options.forEach(function (option, index, array) {
            if (helper.toBool(option.Selected) != option.IsAnswer) {
                result = 'wrong';
                return false;
            }
        });
        return result;
};























    // Save Exam
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.examForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.exam._id) {
        vm.exam.$update(successCallback, errorCallback);
      } else {
        vm.exam.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('exams.view', {
          examId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    }
  }
}());
