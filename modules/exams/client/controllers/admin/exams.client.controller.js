(function () {
  'use strict';

  angular
    .module('exams.admin')
    .controller('ExamsAdminController', ExamsAdminController);

  ExamsAdminController.$inject = ['$scope', '$state', '$window', 'examResolve', 'Authentication', 'Notification', 'SegmentsService'];

  function ExamsAdminController($scope, $state, $window, exam, Authentication, Notification, SegmentsService) {
    var vm = this;

    vm.exam = exam;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    $scope.example1model = [];
    vm.exam.segments = [];

    $scope.example1data = SegmentsService.query();
    // console.log($scope.example1data);

    $scope.exampleSettings = {displayProp: 'question', idProp: '_id'};

    //angular-bootstrap-multiselect

    vm.options = SegmentsService.query(function(data){
      console.log(data);
      return data ;
    });


    // exam category array

    vm.categories = [{
        'category': 'Multi-Optional'
      },
      {
        'category': 'Multi-Correct & Multi-Otional'
      },
      {
        'category': 'True-False '
      }
    ]



    // Remove existing Exam
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.exam.$remove(function () {
          $state.go('admin.exams.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Exam deleted successfully!' });
        });
      }
    }

    // Save Exam
    function save(isValid) {


      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.examForm');
        return false;
      }

      // Create a new exam, or update the current instance
      vm.exam.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.exams.list'); // should we send the User to the list or the updated Exam's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Exam saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Exam save error!' });
      }
    }
  }
}());
