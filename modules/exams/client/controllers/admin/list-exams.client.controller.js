(function () {
  'use strict';

  angular
    .module('exams.admin')
    .controller('ExamsAdminListController', ExamsAdminListController);

  ExamsAdminListController.$inject = ['ExamsService'];

  function ExamsAdminListController(ExamsService) {
    var vm = this;

    vm.exams = ExamsService.query();
  }
}());
