(function() {
  'use strict';

  // Configuring the Articles Admin module
  angular
    .module('exams.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {

    Menus.addMenuItem('topbar', {
      title: 'Exams',
      state: 'admin.exams',
      type: 'dropdown',
      roles: ['admin']
    });

    Menus.addSubMenuItem('topbar', 'admin.exams', {
      title: 'Manage Exams',
      state: 'admin.exams.list'
    });

    Menus.addSubMenuItem('topbar', 'admin.exams', {
      title: 'Create Exams',
      state: 'admin.exams.create'
    });
  }
}());
