(function () {
  'use strict';

  angular
    .module('exams.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.exams', {
        abstract: true,
        url: '/exams',
        template: '<ui-view/>'
      })
      .state('admin.exams.list', {
        url: '',
        templateUrl: '/modules/exams/client/views/admin/list-exams.client.view.html',
        controller: 'ExamsAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.exams.create', {
        url: '/create',
        templateUrl: '/modules/exams/client/views/admin/form-exam.client.view.html',
        controller: 'ExamsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          examResolve: newExam
        }
      })
      .state('admin.exams.edit', {
        url: '/:examId/edit',
        templateUrl: '/modules/exams/client/views/admin/form-exam.client.view.html',
        controller: 'ExamsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin'],
          pageTitle: '{{ examResolve.title }}'
        },
        resolve: {
          examResolve: getExam
        }
      });
  }

  getExam.$inject = ['$stateParams', 'ExamsService'];

  function getExam($stateParams, ExamsService) {
    return ExamsService.get({
      examId: $stateParams.examId
    }).$promise;
  }

  newExam.$inject = ['ExamsService'];

  function newExam(ExamsService) {
    return new ExamsService();
  }
}());
