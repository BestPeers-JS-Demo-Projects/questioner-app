(function () {
  'use strict';

  angular
    .module('exams')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('exams', {
        abstract: true,
        url: '/exams',
        template: '<ui-view/>'
      })
      .state('exams.list', {
        url: '',
        templateUrl: '/modules/exams/client/views/list-exams.client.view.html',
        controller: 'ExamsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Exams List'
        }
      })
      .state('exams.create', {
        url: '/create',
        templateUrl: '/modules/exams/client/views/form-exam.client.view.html',
        controller: 'ExamsController',
        controllerAs: 'vm',
        resolve: {
          examResolve: newExam
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Exams Create'
        }
      })
      .state('exams.edit', {
        url: '/:examId/edit',
        templateUrl: '/modules/exams/client/views/form-exam.client.view.html',
        controller: 'ExamsController',
        controllerAs: 'vm',
        resolve: {
          examResolve: getExam
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Exam {{ examResolve.name }}'
        }
      })
      .state('exams.view', {
        url: '/:examId',
        templateUrl: '/modules/exams/client/views/view-exam.client.view.html',
        controller: 'ExamsController',
        controllerAs: 'vm',
        resolve: {
          examResolve: getExam
        },
        data: {
          pageTitle: 'Exam {{ examResolve.name }}'
        }
      })
      .state('exams.start', {
        url: '/:examId/start',
        templateUrl: '/modules/exams/client/views/start-exam.client.view.html',
        controller: 'ExamsController',
        controllerAs: 'vm',
        resolve: {
          examResolve: getExam
        },
        data: {
          pageTitle: 'Exam {{ examResolve.name }}'
        }
      })
      .state('exams.startSegment', {
        url: '/:examId/start/segment',
        templateUrl: '/modules/exams/client/views/start-segment.client.view.html',
        controller: 'ExamsController',
        controllerAs: 'vm',
        params: {
          examSegment: null
        },
        resolve: {
          examResolve: getExam
        },
        data: {
          pageTitle: 'Exam {{ examResolve.name }}'
        }
      });
  }

  getExam.$inject = ['$stateParams', 'ExamsService'];

  function getExam($stateParams, ExamsService) {
    return ExamsService.get({
      examId: $stateParams.examId
    }).$promise;
  }

  newExam.$inject = ['ExamsService'];

  function newExam(ExamsService) {
    return new ExamsService();
  }
}());
