'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Exam = mongoose.model('Exam'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Exam
 */
exports.create = function(req, res) {
  var exam = new Exam(req.body);
  exam.user = req.user;

  exam.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(exam);
    }
  });
};

/**
 * Show the current Exam
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  var exam = req.exam ? req.exam.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  exam.isCurrentUserOwner = req.user && exam.user && exam.user._id.toString() === req.user._id.toString();

  res.jsonp(exam);
};

/**
 * Update a Exam
 */
exports.update = function(req, res) {
  var exam = req.exam;

  exam = _.extend(exam, req.body);

  exam.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(exam);
    }
  });
};

/**
 * Delete an Exam
 */
exports.delete = function(req, res) {
  var exam = req.exam;

  exam.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(exam);
    }
  });
};

/**
 * List of Exams
 */
exports.list = function(req, res) {
  Exam.find().sort('-created').populate('user', 'displayName').populate('segments', 'name category isMinusMarking time_limit').exec(function(err, exams) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(exams);
    }
  });
};

/**
 * Exam middleware
 */
exports.examByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Exam is invalid'
    });
  }

  Exam.findById(id).populate('user', 'displayName')
  // .populate('segments', 'name category isMinusMarking time_limit')
  .populate({
     path: 'segments',
     // Get friends of friends - populate the 'friends' array for every friend
     populate: { path: 'questions', select: 'question options' }
   })
  .exec(function (err, exam) {
    if (err) {
      return next(err);
    } else if (!exam) {
      return res.status(404).send({
        message: 'No Exam with that identifier has been found'
      });
    }
    req.exam = exam;
    next();
  });
};
