'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Exam Schema
 */
var ExamSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Exam name',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  segments: [{
    type: Schema.ObjectId,
    ref: 'Segment'
  }]
});

mongoose.model('Exam', ExamSchema);
