'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Exam = mongoose.model('Exam'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  exam;

/**
 * Exam routes tests
 */
describe('Exam CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      username: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.username,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new Exam
    user.save(function () {
      exam = {
        name: 'Exam name'
      };

      done();
    });
  });

  it('should be able to save a Exam if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Exam
        agent.post('/api/exams')
          .send(exam)
          .expect(200)
          .end(function (examSaveErr, examSaveRes) {
            // Handle Exam save error
            if (examSaveErr) {
              return done(examSaveErr);
            }

            // Get a list of Exams
            agent.get('/api/exams')
              .end(function (examsGetErr, examsGetRes) {
                // Handle Exams save error
                if (examsGetErr) {
                  return done(examsGetErr);
                }

                // Get Exams list
                var exams = examsGetRes.body;

                // Set assertions
                (exams[0].user._id).should.equal(userId);
                (exams[0].name).should.match('Exam name');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an Exam if not logged in', function (done) {
    agent.post('/api/exams')
      .send(exam)
      .expect(403)
      .end(function (examSaveErr, examSaveRes) {
        // Call the assertion callback
        done(examSaveErr);
      });
  });

  it('should not be able to save an Exam if no name is provided', function (done) {
    // Invalidate name field
    exam.name = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Exam
        agent.post('/api/exams')
          .send(exam)
          .expect(400)
          .end(function (examSaveErr, examSaveRes) {
            // Set message assertion
            (examSaveRes.body.message).should.match('Please fill Exam name');

            // Handle Exam save error
            done(examSaveErr);
          });
      });
  });

  it('should be able to update an Exam if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Exam
        agent.post('/api/exams')
          .send(exam)
          .expect(200)
          .end(function (examSaveErr, examSaveRes) {
            // Handle Exam save error
            if (examSaveErr) {
              return done(examSaveErr);
            }

            // Update Exam name
            exam.name = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing Exam
            agent.put('/api/exams/' + examSaveRes.body._id)
              .send(exam)
              .expect(200)
              .end(function (examUpdateErr, examUpdateRes) {
                // Handle Exam update error
                if (examUpdateErr) {
                  return done(examUpdateErr);
                }

                // Set assertions
                (examUpdateRes.body._id).should.equal(examSaveRes.body._id);
                (examUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a list of Exams if not signed in', function (done) {
    // Create new Exam model instance
    var examObj = new Exam(exam);

    // Save the exam
    examObj.save(function () {
      // Request Exams
      request(app).get('/api/exams')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single Exam if not signed in', function (done) {
    // Create new Exam model instance
    var examObj = new Exam(exam);

    // Save the Exam
    examObj.save(function () {
      request(app).get('/api/exams/' + examObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('name', exam.name);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single Exam with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    request(app).get('/api/exams/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Exam is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single Exam which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent Exam
    request(app).get('/api/exams/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No Exam with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should be able to delete an Exam if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new Exam
        agent.post('/api/exams')
          .send(exam)
          .expect(200)
          .end(function (examSaveErr, examSaveRes) {
            // Handle Exam save error
            if (examSaveErr) {
              return done(examSaveErr);
            }

            // Delete an existing Exam
            agent.delete('/api/exams/' + examSaveRes.body._id)
              .send(exam)
              .expect(200)
              .end(function (examDeleteErr, examDeleteRes) {
                // Handle exam error error
                if (examDeleteErr) {
                  return done(examDeleteErr);
                }

                // Set assertions
                (examDeleteRes.body._id).should.equal(examSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to delete an Exam if not signed in', function (done) {
    // Set Exam user
    exam.user = user;

    // Create new Exam model instance
    var examObj = new Exam(exam);

    // Save the Exam
    examObj.save(function () {
      // Try deleting Exam
      request(app).delete('/api/exams/' + examObj._id)
        .expect(403)
        .end(function (examDeleteErr, examDeleteRes) {
          // Set message assertion
          (examDeleteRes.body.message).should.match('User is not authorized');

          // Handle Exam error error
          done(examDeleteErr);
        });

    });
  });

  it('should be able to get a single Exam that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      username: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.username,
      password: _creds.password,
      provider: 'local'
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new Exam
          agent.post('/api/exams')
            .send(exam)
            .expect(200)
            .end(function (examSaveErr, examSaveRes) {
              // Handle Exam save error
              if (examSaveErr) {
                return done(examSaveErr);
              }

              // Set assertions on new Exam
              (examSaveRes.body.name).should.equal(exam.name);
              should.exist(examSaveRes.body.user);
              should.equal(examSaveRes.body.user._id, orphanId);

              // force the Exam to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the Exam
                    agent.get('/api/exams/' + examSaveRes.body._id)
                      .expect(200)
                      .end(function (examInfoErr, examInfoRes) {
                        // Handle Exam error
                        if (examInfoErr) {
                          return done(examInfoErr);
                        }

                        // Set assertions
                        (examInfoRes.body._id).should.equal(examSaveRes.body._id);
                        (examInfoRes.body.name).should.equal(exam.name);
                        should.equal(examInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  afterEach(function (done) {
    User.remove().exec(function () {
      Exam.remove().exec(done);
    });
  });
});
