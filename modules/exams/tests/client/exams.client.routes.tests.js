(function () {
  'use strict';

  describe('Exams Route Tests', function () {
    // Initialize global variables
    var $scope,
      ExamsService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _ExamsService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      ExamsService = _ExamsService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('exams');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/exams');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('View Route', function () {
        var viewstate,
          ExamsController,
          mockExam;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('exams.view');
          $templateCache.put('modules/exams/client/views/view-exam.client.view.html', '');

          // create mock Exam
          mockExam = new ExamsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Exam Name'
          });

          // Initialize Controller
          ExamsController = $controller('ExamsController as vm', {
            $scope: $scope,
            examResolve: mockExam
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:examId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.examResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            examId: 1
          })).toEqual('/exams/1');
        }));

        it('should attach an Exam to the controller scope', function () {
          expect($scope.vm.exam._id).toBe(mockExam._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('modules/exams/client/views/view-exam.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          ExamsController,
          mockExam;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('exams.create');
          $templateCache.put('modules/exams/client/views/form-exam.client.view.html', '');

          // create mock Exam
          mockExam = new ExamsService();

          // Initialize Controller
          ExamsController = $controller('ExamsController as vm', {
            $scope: $scope,
            examResolve: mockExam
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.examResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/exams/create');
        }));

        it('should attach an Exam to the controller scope', function () {
          expect($scope.vm.exam._id).toBe(mockExam._id);
          expect($scope.vm.exam._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('modules/exams/client/views/form-exam.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          ExamsController,
          mockExam;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('exams.edit');
          $templateCache.put('modules/exams/client/views/form-exam.client.view.html', '');

          // create mock Exam
          mockExam = new ExamsService({
            _id: '525a8422f6d0f87f0e407a33',
            name: 'Exam Name'
          });

          // Initialize Controller
          ExamsController = $controller('ExamsController as vm', {
            $scope: $scope,
            examResolve: mockExam
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:examId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.examResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            examId: 1
          })).toEqual('/exams/1/edit');
        }));

        it('should attach an Exam to the controller scope', function () {
          expect($scope.vm.exam._id).toBe(mockExam._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('modules/exams/client/views/form-exam.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
