(function () {
  'use strict';

  // Quizzes controller
  angular
    .module('quizzes')
    .controller('QuizPlayController', QuizPlayController);

  QuizPlayController.$inject = ['$scope', '$state', '$window', 'Authentication', 'quizResolve'];

  function QuizPlayController ($scope, $state, $window, Authentication, quiz) {
    var vm = this;

    vm.authentication = Authentication;
    vm.quiz = quiz;
    vm.error = null;

console.log(vm.quiz);

  }
}());
